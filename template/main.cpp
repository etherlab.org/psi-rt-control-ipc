/*****************************************************************************
 *
 * Basic Real-Time Application
 *
 ****************************************************************************/

#include "RtMain.h"

#include <exception>
#include <iostream>
#include <sstream>

#include <getopt.h>
#include <libgen.h>  // basename()

using std::cerr;
using std::cout;
using std::endl;

/****************************************************************************/

std::string binaryBaseName;
bool dryRun(false);

#define DEFAULT_PRIO 80
int priority(DEFAULT_PRIO);

/****************************************************************************/

std::string usage()
{
    std::stringstream str;

    str << "Usage: " << binaryBaseName << " [OPTIONS]" << endl
        << "Options:" << endl
        << "  --dry-run   -n         Disable EtherCAT." << endl
        << "  --priority  -p <PRIO>  Priority. Default: " << DEFAULT_PRIO
        << endl
        << "  --help      -h         Show this help." << endl
        << endl;

    return str.str();
}

/****************************************************************************/

void getOptions(int argc, char **argv)
{
    int c, argCount;
    std::stringstream str;

    static struct option longOptions[] = {
            // name,         has_arg,           flag, val
            {"dry-run", no_argument, NULL, 'n'},
            {"priority", required_argument, NULL, 'p'},
            {"help", no_argument, NULL, 'h'},
            {}};

    do {
        c = getopt_long(argc, argv, "np:h", longOptions, NULL);

        switch (c) {
            case 'n':
                dryRun = true;
                break;

            case 'p':
                if (*optarg) {
                    char *end;
                    priority = strtol(optarg, &end, 10);
                    if (*end) {
                        cerr << "Invalid argument!" << endl
                             << usage() << endl;
                        exit(1);
                    }
                }
                else {
                    cerr << "Empty argument!" << endl << usage() << endl;
                    exit(1);
                }
                break;

            case 'h':
                cout << usage();
                exit(0);

            case '?':
                cerr << endl << usage();
                exit(1);

            default:
                break;
        }
    } while (c != -1);

    argCount = argc - optind;

    if (argCount) {
        cerr << binaryBaseName << " takes no arguments!" << endl;
        exit(1);
    }
}

/****************************************************************************/

int main(int argc, char *argv[])
{
    binaryBaseName = basename(argv[0]);
    getOptions(argc, argv);

    try {
        RtMain main(dryRun);
        main.run(priority);
    }
    catch (std::exception &e) {
        cerr << e.what() << endl;
    }
}

/****************************************************************************/
