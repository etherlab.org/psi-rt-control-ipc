/****************************************************************************/

#include "RtMain.h"

#include <iostream>

#include <sys/mman.h>  // mlockall()

using std::cout;
using std::endl;

/****************************************************************************/

RtMain::RtMain(bool dryRun):
    pdserv {"psictl", "0.99"},
    task {pdserv.getPdServ(), 1e-3},
    master {pdserv.getPdServ(),
            task.getPdTask(),
            "/Master",
            0,
            std::bind(&ClockController::getApplicationTime, &clock),
            dryRun},
    io {pdserv.getPdServ(), task.getPdTask(), "/Io", &master},
    clock {pdserv,
           task,
           master,
           "/ClockControl",
           [this](int64_t &diff) {
               if (!io.el6688.getConnected())
                   return false;
               diff = io.el6688.getTimestampDifference();
               return true;
           },
           [this](uint64_t &ts) {
               if (!io.el6688.getConnected())
                   return false;
               ts = io.el6688.getExternalTimestamp();
               return ts != 0;
           }},
    motorEnable {false},
    motorReset {false},
    motorTargetVelocity {0}
{
    pdserv_parameter(
            pdserv.getPdServ(), "/Motor/Enable", 0666, pd_boolean_T,
            &motorEnable, 1, NULL, NULL, NULL);
    pdserv_parameter(
            pdserv.getPdServ(), "/Motor/Reset", 0666, pd_boolean_T,
            &motorReset, 1, NULL, NULL, NULL);
    pdserv_parameter(
            pdserv.getPdServ(), "/Motor/TargetVelocity", 0666, pd_sint16_T,
            &motorTargetVelocity, 1, NULL, NULL, NULL);
}

/****************************************************************************/

RtMain::~RtMain()
{
}

/****************************************************************************/

void RtMain::run(int priority)
{
    cout << "Preparing PdServ..." << endl;
    pdserv.prepare();

    cout << "Locking memory..." << endl;
    if (mlockall(MCL_CURRENT | MCL_FUTURE) == -1) {
        throw std::runtime_error("mlockall() failed");
    }

    cout << "Running realtime task..." << endl;
    task.run(std::bind(&RtMain::update, this), priority);
}

/****************************************************************************/

void RtMain::update()
{
    master.receive();
    io.updateInputs(false);  // FIXME ackMessages

    clock.update();
    io.el7041.setEnable(motorEnable);
    io.el7041.setReset(motorReset);
    io.el7041.setTargetVelocity(motorTargetVelocity);

    io.updateOutputs();
    master.send();
}

/****************************************************************************/
