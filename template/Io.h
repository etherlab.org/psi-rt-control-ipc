/****************************************************************************/

#ifndef IO_H
#define IO_H

/****************************************************************************/

#include <PsiEtherCATToolkit/ethercat/Domain.h>
#include <PsiEtherCATToolkit/ethercat/EL6688.h>
#include <PsiEtherCATToolkit/ethercat/EL704x.h>
#include <PsiEtherCATToolkit/ethercat/ELM3xxx.h>

#include <pdserv.h>

/****************************************************************************/

class Io
{
  public:
    Io(pdserv *, pdtask *, const std::string &, Master *);

    void updateInputs(bool);
    void updateOutputs();

    bool domainValid() const { return domain.isValid(); }

  private:
    Domain domain;

  public:
    EL704x el7041;
    EL6688 el6688;
    ELM3xxx elm3004;
};

/****************************************************************************/

#endif

/****************************************************************************/
