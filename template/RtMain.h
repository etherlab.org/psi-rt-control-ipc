/****************************************************************************/

#ifndef RTMAIN_H
#define RTMAIN_H

/****************************************************************************/

#include "Io.h"
#include <PsiEtherCATToolkit/ClockController.h>
#include <PsiEtherCATToolkit/PdServ.h>
#include <PsiEtherCATToolkit/Task.h>
#include <PsiEtherCATToolkit/ethercat/Master.h>

/****************************************************************************/

class RtMain
{
  public:
    RtMain(bool);
    ~RtMain();

    void run(int);

  private:
    uint64_t clockOffset = 0;
    PdServ pdserv;
    Task task;
    Master master;
    Io io;
    ClockController clock;

    bool motorEnable;
    bool motorReset;
    int16_t motorTargetVelocity;

    void update();
};

#endif

/****************************************************************************/
