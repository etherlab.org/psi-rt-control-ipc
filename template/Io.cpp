/****************************************************************************/

#include "Io.h"

#include <PsiEtherCATToolkit/ethercat/Master.h>

/****************************************************************************/

Io::Io(pdserv *pdserv,
       pdtask *task,
       const std::string &prefix,
       Master *master):
    domain(pdserv, task, prefix + "/Domain", master),
    el7041(pdserv,
           task,
           prefix + "/EL7041",
           master,
           0,
           0,
           &domain,
           &domain,
           EL704x::EL7041),
    el6688(pdserv, task, prefix + "/EL6688", master, 0, 4, &domain),
    elm3004(pdserv,
            task,
            prefix + "/ELM3004",
            master,
            0,
            5,
            &domain,
            ELM3xxx::ELM3004)
{
    el7041.configure();

    master->activate();
    domain.updateDataPointer();
}

/****************************************************************************/

void Io::updateInputs(bool ackMessages)
{
    domain.process(false);  // FIXME resetStats

    el7041.updateInputs();
    el6688.updateInputs();
    elm3004.updateInputs();
}

/****************************************************************************/

void Io::updateOutputs()
{
    el7041.updateOutputs();

    domain.queue();
}

/****************************************************************************/
