# PSI Realtime Control IPC Template

This is a template project for realtime control systems at the Paul Scherrer
Institute (PSI).

## Installing pre-commit hooks

This project uses [pre-commit](https://pre-commit.com) to execute specific
checks before code can be committed. Please execute `pre-commit install` to
install the respective commit hooks.
