/****************************************************************************/

#ifndef ETHERCAT_H
#define ETHERCAT_H

#include <PsiEtherCATToolkit/config.h>

#include <pdserv.h>

#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
#include <ecrt.h>  // EtherCAT realtime interface
#else
struct ec_master_t;
#endif

#include <functional>
#include <string>

#include <inttypes.h>

/****************************************************************************/

class Master
{
  public:
    Master(pdserv *,
           pdtask *,
           const std::string &,
           unsigned int,
           std::function<uint64_t()> get_clock,
           bool = false);
    ~Master();

    void activate();
    void receive();
    void send();

    bool hasMaster() const { return master != nullptr; }
    ec_master_t *getMaster() const { return master; }

    bool dcDeviationGood() const { return dc_deviation_good; }

  private:
    ec_master_t *master;
    unsigned int sync_cycles;
    unsigned int sync_ref_counter;
    unsigned int slaves_responding;
    uint8_t al_states;
    bool link_up;
    double dc_deviation;
    double limit_dc_deviation = 2e-6;
    bool dc_deviation_good;
    std::function<uint64_t()> get_clock;
};

#endif

/****************************************************************************/
