/****************************************************************************/

#ifndef DOMAIN_H
#define DOMAIN_H

/****************************************************************************/

#include <PsiEtherCATToolkit/config.h>

#include <pdserv.h>

#include <cstdint>
#include <string>

#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
#include <ecrt.h>  // EtherCAT realtime interface
#endif

/****************************************************************************/

class Master;

class Domain
{
  public:
    Domain(pdserv *, pdtask *, const std::string &prefix, Master *);

    void updateDataPointer();
    void process(bool);
    void queue();

#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    ec_domain_t *getDomain() { return domain; }
#endif
    uint8_t *getData() const { return data; }
    bool isValid() const { return valid; }

  private:
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    ec_domain_t *domain;
    ec_domain_state_t state;
#endif
    bool valid;
    uint8_t *data;
    uint32_t badCycles;
};

#endif

/****************************************************************************/
