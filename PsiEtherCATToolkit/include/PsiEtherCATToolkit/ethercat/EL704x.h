/****************************************************************************/

#ifndef EL704xH
#define EL704xH

#include <PsiEtherCATToolkit/config.h>

#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
#include <ecrt.h>  // EtherCAT realtime interface
#endif

#include <pdserv.h>

#include <cstdint>
#include <string>
#include <vector>

/****************************************************************************/

class Master;
class Domain;

/****************************************************************************/

/** Beckhoff EL704x stepper motor terminals.
 */
class EL704x
{
  public:
    enum Type { EL7041, NumTypes };

    EL704x(pdserv *,
           pdtask *,
           const std::string &,
           Master *,
           uint16_t,
           uint16_t,
           Domain *,
           Domain *,
           Type);
    ~EL704x();

    void configure();

    void updateInputs();
    bool getReadyToEnable() const;
    bool getReady() const;
    bool getWarning() const;
    bool getError() const;
    int16_t getCounter() const;

    void setEnable(bool);
    void setReset(bool);
    void setReduceTorque(bool);
    void setTargetVelocity(int16_t);
    void updateOutputs() const;

  private:
    struct TypeDetails;
    static const TypeDetails typeDetails[];
    const TypeDetails &type;
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    ec_slave_config_t *sc;
#endif
    Domain *const domainIn;
    Domain *const domainOut;

    // inputs
    int offReadyToEnable;
    unsigned int bitOffReadyToEnable;
    bool readyToEnable;

    int offReady;
    unsigned int bitOffReady;
    bool ready;

    int offWarning;
    unsigned int bitOffWarning;
    bool warning;

    int offError;
    unsigned int bitOffError;
    bool error;

    int offCounter;
    int16_t counter;

    // outputs
    int offEnable;
    unsigned int bitOffEnable;
    bool enable;

    int offReset;
    unsigned int bitOffReset;
    bool reset;

    int offReduceTorque;
    unsigned int bitOffReduceTorque;
    bool reduceTorque;

    int offTargetVelocity;
    int16_t targetVelocity;
};

/****************************************************************************/

#endif

/****************************************************************************/
