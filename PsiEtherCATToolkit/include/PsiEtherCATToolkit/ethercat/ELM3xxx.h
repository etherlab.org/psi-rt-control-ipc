/****************************************************************************/

#ifndef ELM3xxxH
#define ELM3xxxH

#include <PsiEtherCATToolkit/config.h>

#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
#include <ecrt.h>  // EtherCAT realtime interface
#endif

#include <pdserv.h>

#include <string>
#include <vector>

#include <stdint.h>

/****************************************************************************/

class Master;
class Domain;

/****************************************************************************/

/** Beckhoff ELM3xxx analog measuring terminals.
 */
class ELM3xxx
{
  public:
    enum Type { ELM3004, NumTypes };

    ELM3xxx(pdserv *,
            pdtask *,
            const std::string &,
            Master *,
            uint16_t,
            uint16_t,
            Domain *,
            Type);
    ~ELM3xxx();

    void configure();

    void updateInputs();
    double getValue(unsigned int) const;

  private:
    struct TypeDetails;
    static const TypeDetails typeDetails[];
    const TypeDetails &type;
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    ec_slave_config_t *sc;
#endif
    Domain *const domainIn;

    // cyclic data
    std::vector<int> offValue;
    std::vector<double> value;
    std::vector<double> scale;
};

/****************************************************************************/

#endif

/****************************************************************************/
