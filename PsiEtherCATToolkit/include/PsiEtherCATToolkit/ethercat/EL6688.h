/****************************************************************************/

#ifndef EL6688H
#define EL6688H

#include <PsiEtherCATToolkit/Globals.h>

#include <PsiEtherCATToolkit/config.h>

#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
#include <ecrt.h>  // EtherCAT realtime interface
#endif

#include <pdserv.h>

#include <cstdint>
#include <string>
#include <vector>

/****************************************************************************/

class Master;
class Domain;

/****************************************************************************/

/** Beckhoff EL6688 PTP terminal.
 */
class EL6688
{
  public:
    EL6688(pdserv *,
           pdtask *,
           const std::string &,
           Master *,
           uint16_t,
           uint16_t,
           Domain *);
    ~EL6688();

    void configure();

    void updateInputs();
    bool getConnected() const;
    // in DC Epoch (relative to 2000-01-01)
    DcNanosecs getInternalTimestamp() const;
    // in DC Epoch (relative to 2000-01-01)
    DcNanosecs getExternalTimestamp() const;
    /**
     * \return External minus internal timestamp.
     */
    int64_t getTimestampDifference() const;

  private:
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    ec_slave_config_t *sc;
    ec_master_t *master;
#endif
    Domain *const domainIn;

    // cyclic data
    int offNotConnected;
    unsigned int bitOffNotConnected;
    bool connected;
    int offInternalTimestamp;
    uint64_t internalTimestamp;
    int offExternalTimestamp;
    uint64_t externalTimestamp;
    int64_t timestampDifference;
    uint16_t position;
};

/****************************************************************************/

#endif

/****************************************************************************/
