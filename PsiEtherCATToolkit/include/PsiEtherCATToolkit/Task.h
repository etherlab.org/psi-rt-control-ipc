/****************************************************************************/

#ifndef TASK_H
#define TASK_H

#include <pdserv.h>

#include <functional>

#include <time.h>  // struct timespec

/****************************************************************************/

class Task
{
  public:
    Task(struct pdserv *, double);
    ~Task();

    struct pdtask *getPdTask() const { return pdtask; }

    typedef std::function<void()> Function;
    void run(Function, int = 80, size_t = 8192);


    const double period;

  private:
    struct pdserv *pdserv;
    struct pdtask *pdtask;

    typedef std::pair<Task *, Function> PrivData;
    static void *staticCyclicTask(void *);
    void *cyclicTask(Function);
    void inc(struct timespec *) const;
};

#endif

/****************************************************************************/
