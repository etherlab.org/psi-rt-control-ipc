#ifndef PSIECTOOLKIT_GLOBALS_H
#define PSIECTOOLKIT_GLOBALS_H

#include <cstdint>
#include <ratio>

using DcNanosecs = std::uint64_t;
using UnixNanosecs = std::uint64_t;


constexpr int LeapSeconds = 37;
constexpr std::uint64_t UnixToDcEpochOffsetNanoseconds =
        (946684800 - LeapSeconds) * std::nano::den;

#endif
