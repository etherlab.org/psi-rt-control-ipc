/****************************************************************************/

#ifndef PDSERV_CXX_H
#define PDSERC_CXX_H

#include <pdserv.h>

#include <string>

/****************************************************************************/

class PdServ
{
  public:
    PdServ(const std::string &, const std::string &);
    ~PdServ();

    void prepare();

    struct pdserv *getPdServ() const { return pdserv; }

  private:
    struct pdserv *pdserv;
};

/****************************************************************************/

#endif

/****************************************************************************/
