#ifndef CLOCKCONTROLLER_H
#define CLOCKCONTROLLER_H

#include <cstdint>
#include <functional>
#include <stdexcept>
#include <string>
#include <time.h>

#include "BoundedInteger.h"
#include "Globals.h"

class Master;
class PdServ;
class Task;

struct OutOfSyncException : std::runtime_error
{
    using runtime_error::runtime_error;
};

class ClockController
{
  public:
    ClockController(
            PdServ &,
            Task &,
            Master &master,
            const std::string &prefix,
            std::function<bool(int64_t &)> getDifference,
            std::function<bool(DcNanosecs &)> getExternalTimestamp);

    enum State {
        Init,
        RoughDriftCompensatingGtZero,
        RoughDriftCompensatingLtZero,
        CheckRoughAdjust,
        FineDriftCompensating,
        DeadZone,
    };

    static constexpr clockid_t ClockId = CLOCK_MONOTONIC_RAW;
    static constexpr int DriftRatio = 512;
    static constexpr int MaxAllowedBounces = 5;


    /**
     *
     */
    void update();

    DcNanosecs getApplicationTime() const;


  private:
    bool checkInitialAdjustment();

    void adjustRoughDrift();
    void checkRoughAdjust();
    void adjustFineDrift();

    bool dcLongEnoughGood();

    bool updateRelativeDifference();

    Master &master;
    std::function<bool(int64_t &)> getDifference;
    // in DC Epoch
    std::function<bool(DcNanosecs &)> getExternalTimestamp;
    const int cycles1s;
    const uint64_t internalToTaiOffset;

    int32_t state = State::Init;
    int32_t clockDriftAtLastZeroCrossingUp = 0,
            clockDriftAtLastZeroCrossingDown = 0;
    uint64_t clockOffset = 0;
    int64_t relativeDifference = 0;
    int dcGoodCounter = 0;
    int adjustCounter = 0;
    double deadZone = 2e-6;
    bool allowAdjustClock = false;
    int ptpBounceCounter = 0;

    BoundedInteger<int32_t> clockDrift;
};


#endif  // CLOCKCONTROLLER_H
