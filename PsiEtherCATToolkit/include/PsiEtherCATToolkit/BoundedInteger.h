#ifndef BOUNDED_INTERGER_H
#define BOUNDED_INTERGER_H

template <class T> class BoundedInteger
{
  public:
    explicit BoundedInteger(T limit): limit_(limit) {}

    operator T() const { return value_; }

    BoundedInteger &operator+=(T v)
    {
        value_ += v;
        adjustToLimit();
        return *this;
    }
    BoundedInteger &operator-=(T v)
    {
        value_ -= v;
        adjustToLimit();
        return *this;
    }

    BoundedInteger &operator=(T v)
    {
        value_ = v;
        adjustToLimit();
        return *this;
    }

    const void *addr() const { return &value_; }

    T limit() const { return limit_; }

  private:
    T value_ = 0;
    const T limit_;

    void adjustToLimit()
    {
        if (value_ > limit_)
            value_ = limit_;
        else if (value_ < -limit_)
            value_ = -limit_;
    }
};

#endif  // BOUNDED_INTERGER_H
