/****************************************************************************/

#include <PsiEtherCATToolkit/Task.h>

#include <cstdint>
#include <cstring>  // strerror()
#include <sstream>
#include <stdexcept>

#include <limits.h>
#include <pthread.h>
#include <sched.h>

/****************************************************************************/

#define NSEC_PER_SEC 1000000000  // Nanoseconds per second

#define DIFF_NS(A, B) \
    (((B).tv_sec - (A).tv_sec) * NSEC_PER_SEC + (B).tv_nsec - (A).tv_nsec)

/****************************************************************************/

Task::Task(struct pdserv *pdserv, double period):
    period(period), pdserv(pdserv), pdtask(nullptr)
{
    /* Create a PdServ task. */
    if (!(pdtask = pdserv_create_task(pdserv, period, NULL))) {
        throw std::runtime_error("Failed to create task.");
    }
}

/****************************************************************************/

Task::~Task()
{
}

/****************************************************************************/

void Task::run(std::function<void()> function, int priority, size_t stackSize)
{
    int ret(0);
    pthread_attr_t attr;
    struct sched_param param;

    /* Initialize pthread attributes (default values) */
    ret = pthread_attr_init(&attr);
    if (ret) {
        throw std::runtime_error("Failed to init pthread attributes.");
    }

    /* Set a specific stack size  */
    ret = pthread_attr_setstacksize(&attr, PTHREAD_STACK_MIN + stackSize);
    if (ret) {
        throw std::runtime_error("Failed to set thread stack size.");
    }

    /* Set scheduler policy and priority of pthread */
    ret = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
    if (ret) {
        throw std::runtime_error("Failed to set thread scheduling policy.");
    }

    param.sched_priority = priority;

    ret = pthread_attr_setschedparam(&attr, &param);
    if (ret) {
        throw std::runtime_error(
                "Failed to set thread scheduler parameters.");
    }

    /* Use scheduling parameters of attr */
    ret = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
    if (ret) {
        throw std::runtime_error("Failed to set thread inherited scheduler.");
    }

    /* Create a pthread with specified attributes */
    pthread_t thread;
    PrivData privData {this, function};
    ret = pthread_create(&thread, &attr, staticCyclicTask, &privData);
    if (ret) {
        throw std::runtime_error("Failed to create thread.");
    }

    ret = pthread_attr_destroy(&attr);
    if (ret != 0) {
        throw std::runtime_error("Failed to destroy thread attributes.");
    }

    ret = pthread_setname_np(thread, "rt-thread");
    if (ret) {
        throw std::runtime_error("Failed to set thread name.");
    }

    /* Join the thread and wait until it is done */
    ret = pthread_join(thread, NULL);
    if (ret) {
        throw std::runtime_error("Failed to join thread.");
    }
}

/****************************************************************************/

void *Task::staticCyclicTask(void *data)
{
    PrivData *privData(static_cast<PrivData *>(data));
    return privData->first->cyclicTask(privData->second);
}

/****************************************************************************/

void *Task::cyclicTask(Function function)
{
    struct timespec worldTime, startTime, prevStartTime, endTime, nextPeriod;
    uint32_t execNs, measuredPeriodNs;
    int ret;

    /* find timestamp to first run */
    clock_gettime(CLOCK_MONOTONIC, &nextPeriod);
    prevStartTime = nextPeriod;

    while (1) {
        /* Take start times */
        clock_gettime(CLOCK_MONOTONIC, &startTime);
        clock_gettime(CLOCK_REALTIME, &worldTime);

        /* Calculate task statistics */
        measuredPeriodNs = DIFF_NS(prevStartTime, startTime);
        prevStartTime = startTime;

        /* Run actual cyclic function */
        function();

        /* Update PdServ */
        pdserv_update(pdtask, &worldTime);

        /* Update task statistics */
        clock_gettime(CLOCK_MONOTONIC, &endTime);
        execNs = DIFF_NS(startTime, endTime);
        pdserv_update_statistics(
                pdtask, execNs / 1e9, measuredPeriodNs / 1e9, 0);

        inc(&nextPeriod);
        ret = clock_nanosleep(
                CLOCK_MONOTONIC, TIMER_ABSTIME, &nextPeriod, NULL);
        if (ret) {
            std::stringstream err;
            err << "clock_nanosleep(): " << strerror(ret);
            throw std::runtime_error(err.str());
        }
    }

    return NULL;
}

/****************************************************************************/

void Task::inc(struct timespec *t) const
{
    t->tv_nsec += period * 1e9;

    while (t->tv_nsec >= NSEC_PER_SEC) {
        /* timespec nsec overflow */
        t->tv_sec++;
        t->tv_nsec -= NSEC_PER_SEC;
    }
}

/****************************************************************************/
