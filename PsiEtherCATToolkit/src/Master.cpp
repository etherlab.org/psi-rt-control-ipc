/****************************************************************************/

#include <PsiEtherCATToolkit/ethercat/Master.h>

#include <exception>
#include <iostream>
#include <sstream>

#include <arpa/inet.h>  // ntohs()

/****************************************************************************/

Master::Master(
        pdserv *pdserv,
        pdtask *task,
        const std::string &prefix,
        unsigned int index,
        std::function<uint64_t()> get_clock,
        bool dry):
    master(nullptr),
    sync_cycles(1),
    sync_ref_counter(0),
    slaves_responding(0U),
    al_states(0x00),
    link_up(false),
    dc_deviation(0.0),
    get_clock(get_clock)
{
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    if (not dry) {
        if (!(master = ecrt_request_master(index))) {
            std::stringstream err;
            err << "Requesting master " << index << " failed!";
            throw std::runtime_error(err.str());
        }
    }
#endif

    pdserv_parameter(
            pdserv, (prefix + "/SyncCycles").c_str(), 0666, pd_uint32_T,
            &sync_cycles, 1, NULL, NULL, NULL);
    pdserv_parameter(
            pdserv, (prefix + "/DcDeviationLimit").c_str(), 0666, pd_double_T,
            &limit_dc_deviation, 1, NULL, NULL, NULL);
    pdserv_signal(
            task, 1, (prefix + "/SyncRefCounter").c_str(), pd_uint32_T,
            &sync_ref_counter, 1, NULL);
    pdserv_signal(
            task, 1, (prefix + "/SlavesResponding").c_str(), pd_uint32_T,
            &slaves_responding, 1, NULL);
    pdserv_signal(
            task, 1, (prefix + "/ApplicationLayerStates").c_str(),
            pd_uint32_T, &al_states, 1, NULL);
    pdserv_signal(
            task, 1, (prefix + "/LinkUp").c_str(), pd_boolean_T, &link_up, 1,
            NULL);
    pdserv_signal(
            task, 1, (prefix + "/DcDeviation").c_str(), pd_double_T,
            &dc_deviation, 1, NULL);
    pdserv_signal(
            task, 1, (prefix + "/DcDeviationGood").c_str(), pd_boolean_T,
            &dc_deviation_good, 1, NULL);
}

/****************************************************************************/

Master::~Master()
{
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    ecrt_release_master(master);
#endif
}

/****************************************************************************/

void Master::activate()
{
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    if (master) {
        if (ecrt_master_activate(master)) {
            throw std::runtime_error("Failed to activate master!");
        }
    }
#endif
}

/****************************************************************************/

void Master::receive()
{
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    if (master) {
        // receive datagrams
        ecrt_master_receive(master);

        ec_master_state_t state;
        ecrt_master_state(master, &state);
        slaves_responding = state.slaves_responding;
        al_states = state.al_states;
        link_up = state.link_up;

        dc_deviation = ecrt_master_sync_monitor_process(master) * 1e-9;
        dc_deviation_good = abs(dc_deviation) < limit_dc_deviation;
    }
#endif
}

/****************************************************************************/

void Master::send()
{
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    if (master) {
        ecrt_master_application_time(master, get_clock());

        if (!sync_ref_counter) {
            ecrt_master_sync_reference_clock(master);
        }
        ecrt_master_sync_slave_clocks(master);
        ecrt_master_sync_monitor_queue(master);

        ecrt_master_send(master);
    }
#endif

    if (sync_ref_counter) {
        sync_ref_counter--;
    }
    else {
        sync_ref_counter = sync_cycles > 0 ? sync_cycles - 1 : 0;
    }
}
