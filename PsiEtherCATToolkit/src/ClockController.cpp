#include <PsiEtherCATToolkit/ClockController.h>

#include <PsiEtherCATToolkit/PdServ.h>
#include <PsiEtherCATToolkit/Task.h>
#include <PsiEtherCATToolkit/ethercat/Master.h>

#include <time.h>

#include <ratio>
#include <stdexcept>

#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
#include <ecrt.h>  // EtherCAT realtime interface
#endif

static uint64_t toNs(const ::timespec &ts)
{
    return ts.tv_nsec + std::nano::den * ts.tv_sec;
}

static uint64_t taiDifference(clockid_t clk)
{
    ::timespec t_tai, t;
    if (clock_gettime(clk, &t) || clock_gettime(CLOCK_TAI, &t_tai))
        throw std::runtime_error("could not initialize clock offset");

    return toNs(t_tai) - toNs(t);
}

ClockController::ClockController(
        PdServ &pdserv,
        Task &task,
        Master &master,
        const std::string &prefix,
        std::function<bool(int64_t &)> getDifference,
        std::function<bool(uint64_t &)> getExternalTimestamp):
    master(master),
    getDifference(getDifference),
    getExternalTimestamp(getExternalTimestamp),
    cycles1s(1.0 / task.period),
    internalToTaiOffset(taiDifference(ClockId)),
    // max clock drift: 0.8‰
    clockDrift(DriftRatio * task.period * 8e5)
{
    pdserv_parameter(
            pdserv.getPdServ(), (prefix + "/AllowAdjust").c_str(), 0666,
            pd_boolean_T, &allowAdjustClock, 1, nullptr, nullptr, nullptr);
    pdserv_parameter(
            pdserv.getPdServ(), (prefix + "/DeadZone").c_str(), 0666,
            pd_double_T, &deadZone, 1, nullptr, nullptr, nullptr);
    pdserv_signal(
            task.getPdTask(), 1, (prefix + "/ClockOffset").c_str(),
            pd_sint64_T, &clockOffset, 1, NULL);
    pdserv_signal(
            task.getPdTask(), 1, (prefix + "/ClockDrift").c_str(),
            pd_sint32_T, clockDrift.addr(), 1, NULL);
    pdserv_signal(
            task.getPdTask(), 1, (prefix + "/relativeDifference").c_str(),
            pd_sint64_T, &relativeDifference, 1, NULL);
    pdserv_signal(
            task.getPdTask(), 1, (prefix + "/State").c_str(), pd_sint32_T,
            &state, 1, NULL);
}


bool ClockController::dcLongEnoughGood()
{
    if (!master.dcDeviationGood()) {
        dcGoodCounter = 0;
        return false;
    }
    else if (dcGoodCounter >= cycles1s) {
        return true;
    }
    else {
        ++dcGoodCounter;
        return false;
    }
}

bool ClockController::updateRelativeDifference()
{
    int64_t ptpOffset;
    if (!getDifference(ptpOffset) || ptpOffset == 0)
        return false;
    // debouncing faulty PTP timestamp pairs
    if (ptpOffset > std::nano::den || ptpOffset < -std::nano::den) {
        ++ptpBounceCounter;
        if (ptpBounceCounter >= MaxAllowedBounces * cycles1s)
            throw OutOfSyncException("PTP time signal bounced too much");
        else
            return false;
    }
    ptpBounceCounter = 0;
    relativeDifference = ptpOffset;
    return true;
}

void ClockController::update()
{
    clockOffset += clockDrift;
    const bool dcDeviationGood = dcLongEnoughGood();
    switch (state) {
        case Init:
            if (allowAdjustClock && dcDeviationGood) {
                if (checkInitialAdjustment() && updateRelativeDifference()) {
                    if (relativeDifference > 0)
                        state = RoughDriftCompensatingGtZero;
                    else
                        state = RoughDriftCompensatingLtZero;
                    adjustCounter = cycles1s;
                }
            }
            break;
        case RoughDriftCompensatingGtZero:
        case RoughDriftCompensatingLtZero:
            if (dcDeviationGood)
                adjustRoughDrift();
            break;
        case CheckRoughAdjust:
            if (dcDeviationGood)
                checkRoughAdjust();
            break;
        case FineDriftCompensating:
        case DeadZone:
            if (dcDeviationGood)
                adjustFineDrift();
        default:
            break;
    }
}

DcNanosecs ClockController::getApplicationTime() const
{
    struct timespec t;
    clock_gettime(ClockId, &t);
    const auto clock_tai = toNs(t) + internalToTaiOffset;
    return clock_tai + clockOffset / DriftRatio
            - UnixToDcEpochOffsetNanoseconds;
}

bool ClockController::checkInitialAdjustment()
{
    if (!master.dcDeviationGood())
        return false;

    DcNanosecs externalTimestampDc;
    if (!getExternalTimestamp(externalTimestampDc) || !externalTimestampDc)
        return false;
    const UnixNanosecs externalTimestampUnix =
            externalTimestampDc + UnixToDcEpochOffsetNanoseconds;

    struct timespec t;
    clock_gettime(ClockId, &t);
    const auto clock_tai = toNs(t) + internalToTaiOffset;
    if (clock_tai > externalTimestampUnix) {
        if ((clock_tai - externalTimestampUnix) > 2 * std::nano::den)
            throw std::runtime_error(
                    "Initial synchronization with NTP is not good enough");
    }
    else if ((externalTimestampUnix - clock_tai) > 2 * std::nano::den)
        throw std::runtime_error(
                "Initial synchronization with NTP is not good enough");

    clockDrift = 0;
    adjustCounter = 0;
    return true;
}

void ClockController::checkRoughAdjust()
{
    if (!updateRelativeDifference())
        return;

    if (adjustCounter <= 0) {
        const auto absoluteDiff = std::abs(relativeDifference);
        if (absoluteDiff > 20000) {
            if (relativeDifference > 0)
                state = RoughDriftCompensatingGtZero;
            else
                state = RoughDriftCompensatingLtZero;
        }
        else {
            state = FineDriftCompensating;
            adjustCounter = cycles1s;
            clockDriftAtLastZeroCrossingDown = 0;
            clockDriftAtLastZeroCrossingUp = 0;
        }
    }
    else {
        --adjustCounter;
    }
}

void ClockController::adjustRoughDrift()
{
    if (!updateRelativeDifference())
        return;

    if (adjustCounter <= 0) {
        const auto absoluteDiff = std::abs(relativeDifference);
        const int sign = relativeDifference >= 0 ? 1 : -1;
        adjustCounter = cycles1s;

        if (absoluteDiff < 500)
            return;

        if (allowAdjustClock) {
            if (relativeDifference > 0
                && state == RoughDriftCompensatingLtZero) {
                clockDriftAtLastZeroCrossingUp = clockDrift;
                if (clockDriftAtLastZeroCrossingDown) {
                    clockDrift = clockDrift / 2
                            + clockDriftAtLastZeroCrossingDown / 2;
                    clockDriftAtLastZeroCrossingDown = 0;
                    state = CheckRoughAdjust;
                    adjustCounter = 2 * cycles1s;
                    return;
                }
            }
            else if (
                    relativeDifference < 0
                    && state == RoughDriftCompensatingGtZero) {
                clockDriftAtLastZeroCrossingDown = clockDrift;
                if (clockDriftAtLastZeroCrossingUp) {
                    clockDrift = clockDrift / 2
                            + clockDriftAtLastZeroCrossingUp / 2;
                    clockDriftAtLastZeroCrossingUp = 0;
                    state = CheckRoughAdjust;
                    adjustCounter = 2 * cycles1s;
                    return;
                }
            }
            else if (absoluteDiff > 10000000) {
                clockDrift += sign * 100 * DriftRatio;
            }
            else if (absoluteDiff > 1000000) {
                clockDrift += sign * 10 * DriftRatio;
            }
            else if (absoluteDiff > 10000) {
                clockDrift += sign * DriftRatio;
            }
            else {
                clockDrift += sign * DriftRatio / 2;
            }
        }
        state = sign == 1 ? RoughDriftCompensatingGtZero
                          : RoughDriftCompensatingLtZero;
    }
    else {
        --adjustCounter;
    }
}

void ClockController::adjustFineDrift()
{
    if (!updateRelativeDifference())
        return;

    if (adjustCounter <= 0) {
        adjustCounter = cycles1s;


        if (relativeDifference > deadZone * 1e9) {
            if (allowAdjustClock) {
                if (state == DeadZone) {
                    clockDriftAtLastZeroCrossingUp = clockDrift;
                    if (clockDriftAtLastZeroCrossingDown) {
                        clockDrift = clockDrift / 2
                                + clockDriftAtLastZeroCrossingDown / 2;
                        clockDriftAtLastZeroCrossingDown = 0;
                    }
                }
                else
                    clockDrift += std::min<int64_t>(
                            DriftRatio, relativeDifference / 1024);
            }
            state = FineDriftCompensating;
        }
        else if (relativeDifference < deadZone * -1e9) {
            if (allowAdjustClock) {
                if (state == DeadZone) {
                    clockDriftAtLastZeroCrossingDown = clockDrift;
                    if (clockDriftAtLastZeroCrossingUp) {
                        clockDrift = clockDrift / 2
                                + clockDriftAtLastZeroCrossingUp / 2;
                        clockDriftAtLastZeroCrossingUp = 0;
                    }
                }
                else
                    clockDrift += std::max<int64_t>(
                            -DriftRatio, relativeDifference / 1024);
            }
            state = FineDriftCompensating;
        }
        else if (state != DeadZone) {
            state = DeadZone;
        }
    }
    else {
        --adjustCounter;
    }
}
