/****************************************************************************/

#include <PsiEtherCATToolkit/ethercat/EL6688.h>

#include <PsiEtherCATToolkit/ethercat/Domain.h>
#include <PsiEtherCATToolkit/ethercat/Master.h>

#include <sstream>
#include <stdexcept>
#include <unistd.h>

/****************************************************************************/

/* Master 0, Slave 4, "EL6688"
 * Vendor ID:       0x00000002
 * Product code:    0x1a203052
 * Revision number: 0x00160000
 */

#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT

ec_pdo_entry_info_t el6688_pdo_entries[] = {
        {0x10f4, 0x01, 2},  /* Sync Mode */
        {0x0000, 0x00, 6},  /* Gap */
        {0x0000, 0x00, 5},  /* Gap */
        {0x10f4, 0x0e, 1},  /* Control value update toggle */
        {0x10f4, 0x0f, 1},  /* Time stamp update toggle */
        {0x10f4, 0x10, 1},  /* External device not connected */
        {0x10f4, 0x11, 64}, /* Internal time stamp */
        {0x10f4, 0x12, 64}, /* External time stamp */
        {0x10f4, 0x13, 32}, /* Control Value for DC Master Clock */
};

ec_pdo_info_t el6688_pdos[] = {
        {0x1a00, 9, el6688_pdo_entries + 0}, /*   TxPDO-Map External Sync */
};

ec_sync_info_t el6688_syncs[] = {
        {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
        {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
        {2, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
        {3, EC_DIR_INPUT, 1, el6688_pdos + 0, EC_WD_DISABLE},
        {0xff}};

#endif

/*****************************************************************************
 * public
 ****************************************************************************/

EL6688::EL6688(
        pdserv *pdserv,
        pdtask *task,
        const std::string &prefix,
        Master *master,
        uint16_t alias,
        uint16_t position,
        Domain *domainIn):
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    sc(NULL),
    master(master->getMaster()),
#endif
    domainIn(domainIn),
    offNotConnected(-1),
    connected(false),
    offInternalTimestamp(-1),
    internalTimestamp(0),
    offExternalTimestamp(-1),
    externalTimestamp(0),
    position(position)
{
    pdserv_signal(
            task, 1, (prefix + "/Connected").c_str(), pd_boolean_T,
            &connected, 1, NULL);
    pdserv_signal(
            task, 1, (prefix + "/InternalTimestamp").c_str(), pd_uint64_T,
            &internalTimestamp, 1, NULL);
    pdserv_signal(
            task, 1, (prefix + "/ExternalTimestamp").c_str(), pd_uint64_T,
            &externalTimestamp, 1, NULL);
    pdserv_signal(
            task, 1, (prefix + "/TimestampDifference").c_str(), pd_sint64_T,
            &timestampDifference, 1, NULL);

    if (!master) {
        return;
    }

#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    sc = ecrt_master_slave_config(
            master->getMaster(), alias, position, 0x00000002, 0x1a203052);
    if (!sc) {
        std::stringstream err;
        err << "Failed to get slave configuration.";
        throw std::runtime_error(err.str());
    }

    if (ecrt_slave_config_pdos(sc, EC_END, el6688_syncs)) {
        std::stringstream err;
        err << "Failed to configure PDOs.";
        throw std::runtime_error(err.str());
    }

    offNotConnected = ecrt_slave_config_reg_pdo_entry(
            sc, 0x10f4, 0x10, domainIn->getDomain(), &bitOffNotConnected);
    if (offNotConnected < 0) {
        std::stringstream err;
        err << "Failed to register value PDO entry.";
        throw std::runtime_error(err.str());
    }

    offInternalTimestamp = ecrt_slave_config_reg_pdo_entry(
            sc, 0x10f4, 0x11, domainIn->getDomain(), NULL);
    if (offInternalTimestamp < 0) {
        std::stringstream err;
        err << "Failed to register value PDO entry.";
        throw std::runtime_error(err.str());
    }

    offExternalTimestamp = ecrt_slave_config_reg_pdo_entry(
            sc, 0x10f4, 0x12, domainIn->getDomain(), NULL);
    if (offInternalTimestamp < 0) {
        std::stringstream err;
        err << "Failed to register value PDO entry.";
        throw std::runtime_error(err.str());
    }
#endif
}

/****************************************************************************/

EL6688::~EL6688()
{
}

/****************************************************************************/

void EL6688::configure()
{
}

/****************************************************************************/

void EL6688::updateInputs()
{
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    if (domainIn->getData()) {
        connected = not EC_READ_BIT(
                domainIn->getData() + offNotConnected, bitOffNotConnected);
        internalTimestamp =
                EC_READ_U64(domainIn->getData() + offInternalTimestamp);
        externalTimestamp =
                EC_READ_U64(domainIn->getData() + offExternalTimestamp);
        timestampDifference = externalTimestamp - internalTimestamp;
    }
#endif
}

/****************************************************************************/

bool EL6688::getConnected() const
{
    return connected;
}

/****************************************************************************/

DcNanosecs EL6688::getInternalTimestamp() const
{
    return internalTimestamp;
}

/****************************************************************************/

DcNanosecs EL6688::getExternalTimestamp() const
{
    return externalTimestamp;
}

/****************************************************************************/

int64_t EL6688::getTimestampDifference() const
{
    return timestampDifference;
}

/****************************************************************************/
