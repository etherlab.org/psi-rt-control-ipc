/*****************************************************************************/

#include <PsiEtherCATToolkit/ethercat/Domain.h>

#include <PsiEtherCATToolkit/ethercat/Master.h>

#include <stdexcept>

/*****************************************************************************/

Domain::Domain(
        pdserv *pdserv,
        pdtask *task,
        const std::string &prefix,
        Master *master):
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    domain(NULL),
#endif
    valid(false),
    data(NULL),
    badCycles(0U)
{
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    state.working_counter = 0;
    state.wc_state = EC_WC_ZERO;
    state.redundancy_active = 0;

    pdserv_signal(
            task, 1, (prefix + "/WorkingCounter").c_str(), pd_uint32_T,
            &state.working_counter, 1, NULL);
    pdserv_signal(
            task, 1, (prefix + "/WorkingCounterState").c_str(), pd_uint32_T,
            &state.wc_state, 1, NULL);
    pdserv_signal(
            task, 1, (prefix + "/RedundancyActive").c_str(), pd_uint32_T,
            &state.redundancy_active, 1, NULL);
#endif
    pdserv_signal(
            task, 1, (prefix + "/Valid").c_str(), pd_boolean_T, &valid, 1,
            NULL);
    pdserv_signal(
            task, 1, (prefix + "/BadCycles").c_str(), pd_uint32_T, &badCycles,
            1, NULL);

    if (!master) {
        return;
    }

#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    if (!(domain = ecrt_master_create_domain(master->getMaster()))) {
        throw std::runtime_error("Domain creation failed!");
    }
#endif
}

/*****************************************************************************/

void Domain::updateDataPointer()
{
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    if (domain) {
        data = ecrt_domain_data(domain);
    }
#endif
}

/*****************************************************************************/

void Domain::process(bool resetStats)
{
    if (resetStats) {
        badCycles = 0U;
    }

#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    if (domain) {
        ecrt_domain_process(domain);
        ecrt_domain_state(domain, &state);
        valid = state.wc_state == EC_WC_COMPLETE;
        badCycles += !valid;
    }
#endif
}

/*****************************************************************************/

void Domain::queue()
{
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    if (domain) {
        ecrt_domain_queue(domain);
    }
#endif
}

/*****************************************************************************/
