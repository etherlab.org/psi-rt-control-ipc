/****************************************************************************/

#include <PsiEtherCATToolkit/ethercat/ELM3xxx.h>

#include <PsiEtherCATToolkit/ethercat/Domain.h>
#include <PsiEtherCATToolkit/ethercat/Master.h>

#include <sstream>
#include <stdexcept>

/****************************************************************************/

/* Master 0, Slave 5, "ELM3004"
 * Vendor ID:       0x00000002
 * Product code:    0x50216dc9
 * Revision number: 0x00110000
 */

#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT

ec_pdo_entry_info_t elm3004_pdo_entries[] = {
        {0x6000, 0x01, 8},  /* No of Samples */
        {0x6000, 0x09, 1},  /* Error */
        {0x6000, 0x0a, 1},  /* Underrange */
        {0x6000, 0x0b, 1},  /* Overrange */
        {0x0000, 0x00, 1},  /* Gap */
        {0x6000, 0x0d, 1},  /* Diag */
        {0x6000, 0x0e, 1},  /* TxPDO State */
        {0x6000, 0x0f, 2},  /* Input cycle counter */
        {0x0000, 0x00, 16}, /* Gap */
        {0x6001, 0x01, 32}, /* SubIndex 001 */
        {0x6010, 0x01, 8},  /* No of Samples */
        {0x6010, 0x09, 1},  /* Error */
        {0x6010, 0x0a, 1},  /* Underrange */
        {0x6010, 0x0b, 1},  /* Overrange */
        {0x0000, 0x00, 1},  /* Gap */
        {0x6010, 0x0d, 1},  /* Diag */
        {0x6010, 0x0e, 1},  /* TxPDO State */
        {0x6010, 0x0f, 2},  /* Input cycle counter */
        {0x0000, 0x00, 16}, /* Gap */
        {0x6011, 0x01, 32}, /* SubIndex 001 */
        {0x6020, 0x01, 8},  /* No of Samples */
        {0x6020, 0x09, 1},  /* Error */
        {0x6020, 0x0a, 1},  /* Underrange */
        {0x6020, 0x0b, 1},  /* Overrange */
        {0x0000, 0x00, 1},  /* Gap */
        {0x6020, 0x0d, 1},  /* Diag */
        {0x6020, 0x0e, 1},  /* TxPDO State */
        {0x6020, 0x0f, 2},  /* Input cycle counter */
        {0x0000, 0x00, 16}, /* Gap */
        {0x6021, 0x01, 32}, /* SubIndex 001 */
        {0x6030, 0x01, 8},  /* No of Samples */
        {0x6030, 0x09, 1},  /* Error */
        {0x6030, 0x0a, 1},  /* Underrange */
        {0x6030, 0x0b, 1},  /* Overrange */
        {0x0000, 0x00, 1},  /* Gap */
        {0x6030, 0x0d, 1},  /* Diag */
        {0x6030, 0x0e, 1},  /* TxPDO State */
        {0x6030, 0x0f, 2},  /* Input cycle counter */
        {0x0000, 0x00, 16}, /* Gap */
        {0x6031, 0x01, 32}, /* SubIndex 001 */
};

ec_pdo_info_t elm3004_pdos[] = {
        {0x1a00, 9, elm3004_pdo_entries + 0}, /* PAI TxPDO-Map Status Ch.1 */
        {0x1a01, 1,
         elm3004_pdo_entries + 9}, /* PAI TxPDO-Map Samples 1 Ch.1 */
        {0x1a21, 9, elm3004_pdo_entries + 10}, /* PAI TxPDO-Map Status Ch.2 */
        {0x1a22, 1,
         elm3004_pdo_entries + 19}, /* PAI TxPDO-Map Samples 1 Ch.2 */
        {0x1a42, 9, elm3004_pdo_entries + 20}, /* PAI TxPDO-Map Status Ch.3 */
        {0x1a43, 1,
         elm3004_pdo_entries + 29}, /* PAI TxPDO-Map Samples 1 Ch.3 */
        {0x1a63, 9, elm3004_pdo_entries + 30}, /* PAI TxPDO-Map Status Ch.4 */
        {0x1a64, 1,
         elm3004_pdo_entries + 39}, /* PAI TxPDO-Map Samples 1 Ch.4 */
};

ec_sync_info_t elm3004_syncs[] = {
        {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
        {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
        {2, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
        {3, EC_DIR_INPUT, 8, elm3004_pdos + 0, EC_WD_DISABLE},
        {0xff}};

#endif

/****************************************************************************/

struct ELM3xxx::TypeDetails
{
    uint32_t product_code;
    unsigned int num_channels;
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    ec_sync_info_t *sync;
#endif
};

const ELM3xxx::TypeDetails ELM3xxx::typeDetails[] = {
        {0x50216dc9, 4,  // ELM3004-0000
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
         elm3004_syncs
#endif
        },
};

/*****************************************************************************
 * public
 ****************************************************************************/

ELM3xxx::ELM3xxx(
        pdserv *pdserv,
        pdtask *task,
        const std::string &prefix,
        Master *master,
        uint16_t alias,
        uint16_t position,
        Domain *domainIn,
        Type typeIdx):
    type(typeDetails[typeIdx]),
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    sc(NULL),
#endif
    domainIn(domainIn),
    offValue(type.num_channels, -1),
    value(type.num_channels, 0.0),
    scale(type.num_channels, 1.0)
{
    pdserv_parameter(
            pdserv, (prefix + "/Scale").c_str(), 0666, pd_double_T,
            scale.data(), scale.size(), NULL, NULL, NULL);
    pdserv_signal(
            task, 1, (prefix + "/Value").c_str(), pd_double_T, value.data(),
            value.size(), NULL);

    if (!master) {
        return;
    }

#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    sc = ecrt_master_slave_config(
            master->getMaster(), alias, position, 0x00000002,
            type.product_code);
    if (!sc) {
        std::stringstream err;
        err << "Failed to get slave configuration.";
        throw std::runtime_error(err.str());
    }

    if (ecrt_slave_config_pdos(sc, EC_END, type.sync)) {
        std::stringstream err;
        err << "Failed to configure PDOs.";
        throw std::runtime_error(err.str());
    }

    for (unsigned int c = 0; c < type.num_channels; c++) {
        offValue[c] = ecrt_slave_config_reg_pdo_entry(
                sc, 0x6001 + c * 0x10, 0x01, domainIn->getDomain(), NULL);
        if (offValue[c] < 0) {
            std::stringstream err;
            err << "Failed to register value PDO entry.";
            throw std::runtime_error(err.str());
        }
    }
#endif
}

/****************************************************************************/

ELM3xxx::~ELM3xxx()
{
}

/****************************************************************************/

void ELM3xxx::configure()
{
}

/****************************************************************************/

void ELM3xxx::updateInputs()
{
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    for (unsigned int c = 0; c < type.num_channels; c++) {
        if (domainIn->getData()) {
            // Doc: U_in = PDO * MBE / 7812500
            value[c] =
                    EC_READ_S32(domainIn->getData() + offValue[c]) * scale[c];
        }
    }
#endif
}

/****************************************************************************/

double ELM3xxx::getValue(unsigned int channel) const
{
    if (channel < type.num_channels) {
        return value[channel];
    }
    else {
        return 0.0;
    }
}

/****************************************************************************/
