/****************************************************************************/

#include <PsiEtherCATToolkit/ethercat/EL704x.h>

#include <PsiEtherCATToolkit/ethercat/Domain.h>
#include <PsiEtherCATToolkit/ethercat/Master.h>

#include <sstream>
#include <stdexcept>

/****************************************************************************/

/* Master 0, Slave 3, "EL7041"
 * Vendor ID:       0x00000002
 * Product code:    0x1b813052
 * Revision number: 0x00190000
 */

#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT

ec_pdo_entry_info_t el7041_pdo_entries[] = {
        {0x7000, 0x01, 1},  /* Enable latch C */
        {0x7000, 0x02, 1},  /* Enable latch extern on positive edge */
        {0x7000, 0x03, 1},  /* Set counter */
        {0x7000, 0x04, 1},  /* Enable latch extern on negative edge */
        {0x0000, 0x00, 4},  /* Gap */
        {0x0000, 0x00, 8},  /* Gap */
        {0x7000, 0x11, 16}, /* Set counter value */
        {0x7010, 0x01, 1},  /* Enable */
        {0x7010, 0x02, 1},  /* Reset */
        {0x7010, 0x03, 1},  /* Reduce torque */
        {0x0000, 0x00, 5},  /* Gap */
        {0x0000, 0x00, 8},  /* Gap */
        {0x7010, 0x21, 16}, /* Velocity */
        {0x6000, 0x01, 1},  /* Latch C valid */
        {0x6000, 0x02, 1},  /* Latch extern valid */
        {0x6000, 0x03, 1},  /* Set counter done */
        {0x6000, 0x04, 1},  /* Counter underflow */
        {0x6000, 0x05, 1},  /* Counter overflow */
        {0x0000, 0x00, 2},  /* Gap */
        {0x6000, 0x08, 1},  /* Extrapolation stall */
        {0x6000, 0x09, 1},  /* Status of input A */
        {0x6000, 0x0a, 1},  /* Status of input B */
        {0x6000, 0x0b, 1},  /* Status of input C */
        {0x0000, 0x00, 1},  /* Gap */
        {0x6000, 0x0d, 1},  /* Status of extern latch */
        {0x1c32, 0x20, 1},  /* Sync error */
        {0x0000, 0x00, 1},  /* Gap */
        {0x6000, 0x10, 1},  /* TxPDO Toggle */
        {0x6000, 0x11, 16}, /* Counter value */
        {0x6000, 0x12, 16}, /* Latch value */
        {0x6010, 0x01, 1},  /* Ready to enable */
        {0x6010, 0x02, 1},  /* Ready */
        {0x6010, 0x03, 1},  /* Warning */
        {0x6010, 0x04, 1},  /* Error */
        {0x6010, 0x05, 1},  /* Moving positive */
        {0x6010, 0x06, 1},  /* Moving negative */
        {0x6010, 0x07, 1},  /* Torque reduced */
        {0x0000, 0x00, 1},  /* Gap */
        {0x0000, 0x00, 3},  /* Gap */
        {0x6010, 0x0c, 1},  /* Digital input 1 */
        {0x6010, 0x0d, 1},  /* Digital input 2 */
        {0x6010, 0x0e, 1},  /* Sync error */
        {0x0000, 0x00, 1},  /* Gap */
        {0x6010, 0x10, 1},  /* TxPDO Toggle */
};

ec_pdo_info_t el7041_pdos[] = {
        {0x1600, 7,
         el7041_pdo_entries + 0}, /* ENC RxPDO-Map Control compact */
        {0x1602, 5, el7041_pdo_entries + 7},  /* STM RxPDO-Map Control */
        {0x1604, 1, el7041_pdo_entries + 12}, /* STM RxPDO-Map Velocity */
        {0x1a00, 17,
         el7041_pdo_entries + 13}, /* ENC TxPDO-Map Status compact */
        {0x1a03, 14, el7041_pdo_entries + 30}, /* STM TxPDO-Map Status */
};

ec_sync_info_t el7041_syncs[] = {
        {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
        {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
        {2, EC_DIR_OUTPUT, 3, el7041_pdos + 0, EC_WD_DISABLE},
        {3, EC_DIR_INPUT, 2, el7041_pdos + 3, EC_WD_DISABLE},
        {0xff}};

#endif

/****************************************************************************/

struct EL704x::TypeDetails
{
    uint32_t product_code;
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    ec_sync_info_t *sync;
#endif
};

const EL704x::TypeDetails EL704x::typeDetails[] = {
        {0x1b813052,  // EL7041
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
         el7041_syncs
#endif
        },
};

/*****************************************************************************
 * public
 ****************************************************************************/

EL704x::EL704x(
        pdserv *pdserv,
        pdtask *task,
        const std::string &prefix,
        Master *master,
        uint16_t alias,
        uint16_t position,
        Domain *domainIn,
        Domain *domainOut,
        Type typeIdx):
    type(typeDetails[typeIdx]),
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    sc(nullptr),
#endif
    domainIn(domainIn),
    domainOut(domainOut),
    offReadyToEnable(-1),
    bitOffReadyToEnable(0),
    readyToEnable(false),
    offReady(-1),
    bitOffReady(0),
    ready(false),
    offWarning(-1),
    bitOffWarning(0),
    warning(false),
    offError(-1),
    bitOffError(0),
    error(false),
    offCounter(-1),
    counter(0),
    offEnable(-1),
    bitOffEnable(0),
    enable(false),
    offReset(-1),
    bitOffReset(0),
    reset(false),
    offReduceTorque(-1),
    bitOffReduceTorque(0),
    reduceTorque(false),
    offTargetVelocity(-1),
    targetVelocity(0)
{
    pdserv_signal(
            task, 1, (prefix + "/ReadyToEnable").c_str(), pd_boolean_T,
            &readyToEnable, 1, NULL);
    pdserv_signal(
            task, 1, (prefix + "/Ready").c_str(), pd_boolean_T, &ready, 1,
            NULL);
    pdserv_signal(
            task, 1, (prefix + "/Warning").c_str(), pd_boolean_T, &warning, 1,
            NULL);
    pdserv_signal(
            task, 1, (prefix + "/Error").c_str(), pd_boolean_T, &error, 1,
            NULL);
    pdserv_signal(
            task, 1, (prefix + "/Counter").c_str(), pd_sint16_T, &counter, 1,
            NULL);
    pdserv_signal(
            task, 1, (prefix + "/Enable").c_str(), pd_boolean_T, &enable, 1,
            NULL);
    pdserv_signal(
            task, 1, (prefix + "/Reset").c_str(), pd_boolean_T, &reset, 1,
            NULL);
    pdserv_signal(
            task, 1, (prefix + "/ReduceTorque").c_str(), pd_boolean_T,
            &reduceTorque, 1, NULL);
    pdserv_signal(
            task, 1, (prefix + "/TargetVelocity").c_str(), pd_sint16_T,
            &targetVelocity, 1, NULL);

    if (not master->hasMaster()) {
        return;
    }

#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    sc = ecrt_master_slave_config(
            master->getMaster(), alias, position, 0x00000002,
            type.product_code);
    if (!sc) {
        std::stringstream err;
        err << "Failed to get slave configuration.";
        throw std::runtime_error(err.str());
    }

    if (ecrt_slave_config_pdos(sc, EC_END, type.sync)) {
        std::stringstream err;
        err << "Failed to configure PDOs.";
        throw std::runtime_error(err.str());
    }

    offReadyToEnable = ecrt_slave_config_reg_pdo_entry(
            sc, 0x6010, 0x01, domainIn->getDomain(), &bitOffReadyToEnable);
    if (offReadyToEnable < 0) {
        std::stringstream err;
        err << "Failed to register value PDO entry.";
        throw std::runtime_error(err.str());
    }

    offReady = ecrt_slave_config_reg_pdo_entry(
            sc, 0x6010, 0x02, domainIn->getDomain(), &bitOffReady);
    if (offReady < 0) {
        std::stringstream err;
        err << "Failed to register value PDO entry.";
        throw std::runtime_error(err.str());
    }

    offWarning = ecrt_slave_config_reg_pdo_entry(
            sc, 0x6010, 0x03, domainIn->getDomain(), &bitOffWarning);
    if (offWarning < 0) {
        std::stringstream err;
        err << "Failed to register value PDO entry.";
        throw std::runtime_error(err.str());
    }

    offError = ecrt_slave_config_reg_pdo_entry(
            sc, 0x6010, 0x04, domainIn->getDomain(), &bitOffError);
    if (offError < 0) {
        std::stringstream err;
        err << "Failed to register value PDO entry.";
        throw std::runtime_error(err.str());
    }

    offCounter = ecrt_slave_config_reg_pdo_entry(
            sc, 0x6000, 0x11, domainIn->getDomain(), NULL);
    if (offCounter < 0) {
        std::stringstream err;
        err << "Failed to register value PDO entry.";
        throw std::runtime_error(err.str());
    }

    offEnable = ecrt_slave_config_reg_pdo_entry(
            sc, 0x7010, 0x01, domainOut->getDomain(), &bitOffEnable);
    if (offEnable < 0) {
        std::stringstream err;
        err << "Failed to register value PDO entry.";
        throw std::runtime_error(err.str());
    }

    offReset = ecrt_slave_config_reg_pdo_entry(
            sc, 0x7010, 0x02, domainOut->getDomain(), &bitOffReset);
    if (offEnable < 0) {
        std::stringstream err;
        err << "Failed to register value PDO entry.";
        throw std::runtime_error(err.str());
    }

    offReduceTorque = ecrt_slave_config_reg_pdo_entry(
            sc, 0x7010, 0x03, domainOut->getDomain(), &bitOffReduceTorque);
    if (offReduceTorque < 0) {
        std::stringstream err;
        err << "Failed to register value PDO entry.";
        throw std::runtime_error(err.str());
    }

    offTargetVelocity = ecrt_slave_config_reg_pdo_entry(
            sc, 0x7010, 0x21, domainOut->getDomain(), NULL);
    if (offTargetVelocity < 0) {
        std::stringstream err;
        err << "Failed to register value PDO entry.";
        throw std::runtime_error(err.str());
    }
#endif
}

/****************************************************************************/

EL704x::~EL704x()
{
}

/****************************************************************************/

void EL704x::configure()
{
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    std::stringstream err;
    err << "Failed to configure SDO.";

    if (ecrt_slave_config_sdo8(sc, 0x8000, 0x08, 0)) {  // deactivate filter
        throw std::runtime_error(err.str());
    }
    if (ecrt_slave_config_sdo8(sc, 0x8000, 0x0a, 0)) {  // enable microsteps
        throw std::runtime_error(err.str());
    }
    if (ecrt_slave_config_sdo8(sc, 0x8000, 0x0e, 0)) {  // revert direction
        throw std::runtime_error(err.str());
    }
    if (ecrt_slave_config_sdo16(sc, 0x8010, 0x01, 550)) {  // max current
        throw std::runtime_error(err.str());
    }
    if (ecrt_slave_config_sdo16(sc, 0x8010, 0x02, 550)) {  // reduced current
        throw std::runtime_error(err.str());
    }
    if (ecrt_slave_config_sdo16(sc, 0x8010, 0x03, 19000)) {  // nom. voltage
        throw std::runtime_error(err.str());
    }
    if (ecrt_slave_config_sdo16(sc, 0x8010, 0x04, 340)) {  // coil resistance
        throw std::runtime_error(err.str());
    }
    if (ecrt_slave_config_sdo16(sc, 0x8010, 0x06, 200)) {  // # full steps
        throw std::runtime_error(err.str());
    }
    if (ecrt_slave_config_sdo8(sc, 0x8012, 0x01, 1)) {  // direct speed mode
        throw std::runtime_error(err.str());
    }
    if (ecrt_slave_config_sdo8(sc, 0x8012, 0x05, 1)) {  // speed range 2000
        throw std::runtime_error(err.str());
    }
    if (ecrt_slave_config_sdo8(sc, 0x8012, 0x08, 1)) {  // internal feedback
        throw std::runtime_error(err.str());
    }
#endif
}

/****************************************************************************/

void EL704x::updateInputs()
{
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    if (domainIn->getData()) {
        readyToEnable = EC_READ_BIT(
                domainIn->getData() + offReadyToEnable, bitOffReadyToEnable);
        ready = EC_READ_BIT(domainIn->getData() + offReady, bitOffReady);
        warning =
                EC_READ_BIT(domainIn->getData() + offWarning, bitOffWarning);
        error = EC_READ_BIT(domainIn->getData() + offError, bitOffError);
        counter = EC_READ_S16(domainIn->getData() + offCounter);
    }
#endif
}

/****************************************************************************/

bool EL704x::getReadyToEnable() const
{
    return readyToEnable;
}

/****************************************************************************/

bool EL704x::getReady() const
{
    return ready;
}

/****************************************************************************/

bool EL704x::getWarning() const
{
    return warning;
}

/****************************************************************************/

bool EL704x::getError() const
{
    return error;
}

/****************************************************************************/

int16_t EL704x::getCounter() const
{
    return counter;
}

/****************************************************************************/

void EL704x::setEnable(bool e)
{
    enable = e;
}

/****************************************************************************/

void EL704x::setReset(bool r)
{
    reset = r;
}

/****************************************************************************/

void EL704x::setReduceTorque(bool r)
{
    reduceTorque = r;
}

/****************************************************************************/

void EL704x::setTargetVelocity(int16_t v)
{
    targetVelocity = v;
}

/****************************************************************************/

void EL704x::updateOutputs() const
{
#if PSIETHERCATTOOLKIT_BUILD_ETHERCAT
    if (domainOut->getData()) {
        EC_WRITE_BIT(domainOut->getData() + offEnable, bitOffEnable, enable);
        EC_WRITE_BIT(domainOut->getData() + offReset, bitOffReset, reset);
        EC_WRITE_BIT(
                domainOut->getData() + offReduceTorque, bitOffReduceTorque,
                reduceTorque);
        EC_WRITE_S16(
                domainOut->getData() + offTargetVelocity, targetVelocity);
    }
#endif
}

/****************************************************************************/
