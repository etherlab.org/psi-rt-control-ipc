/****************************************************************************/

#include <PsiEtherCATToolkit/PdServ.h>

#include <stdexcept>

/****************************************************************************/

PdServ::PdServ(const std::string &appName, const std::string &appVersion)
{
    if (!(pdserv =
                  pdserv_create(appName.c_str(), appVersion.c_str(), NULL))) {
        throw std::runtime_error("Failed to init PdServ.");
    }
}

/****************************************************************************/

PdServ::~PdServ()
{
    pdserv_exit(pdserv);
}

/****************************************************************************/

void PdServ::prepare()
{
    if (pdserv_prepare(pdserv)) {
        throw std::runtime_error("Failed to prepare PdServ.");
    }
}

/****************************************************************************/
