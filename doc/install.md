vim: tw=78

# Installation of openSUSE as an operating system

The openSUSE Leap distribution has been chosen as base for the realtime
control template, because of its good availability and the availability of
[EtherLab](https://gitlab.org/etherlab.org) packages in the [Open Build
Service](https://build.opensuse.org/project/show/science:EtherLab).

## Preparation of an USB device

First we need to download the current openSUSE installation file and prepare
our installation medium. We will install over a USB-Stick.

### Network or offline installer

For the online installer that downloads all necessary packages please download
[openSUSE-Leap-15.3-NET-aarch64-Media.iso](https://download.opensuse.org/distribution/leap/15.3/iso/openSUSE-Leap-15.3-NET-aarch64-Media.iso)

For a complete installer that does not need a network connection during the
setup please download
[openSUSE-Leap-15.3-DVD-x86_64-Current.iso](https://download.opensuse.org/distribution/leap/15.3/iso/openSUSE-Leap-15.3-DVD-x86_64-Current.iso)
This needs more space on your USB device, but 8 GB will be enough.

### Create bootable usb device

To see how you can create a bootable USB device please refer to the openSUSE
documentation at
[https://get.opensuse.org/leap](https://get.opensuse.org/leap). There you can
look at

[How to create a bootable USB stick on Linux](https://en.opensuse.org/SDB:Live_USB_stick)

[How to create a Bootable USB stick on Windows](https://en.opensuse.org/SDB:Create_a_Live_USB_stick_using_Windows)

[How to create a bootable USB stick on OS X](https://en.opensuse.org/SDB:Create_a_Live_USB_stick_using_Mac_OS_x)

Now that we have created the USB stick we can plug it in our Beckhoff CX5140
IPC. After we boot we press `F7` to access the boot menu. Here we can choose
our USB device and the installation process will begin.

## The openSUSE installer

### Starting the installer

![Menu that shows after selection of the USB device](screenshots/bootusb.png)

You will be greeted with a sub menu where you will need to choose install. Now
the Installer will start.

### Choosing the right language

![Keyboard layout and language](screenshots/welcome.png)

In this menu you can choose your language and keyboard layout.

### Setting up Network

As long as DHCP is active in your network you should not need to configure the
Network manually. The installer will usually just skip over this point. If you
have disabled DHCP in your network you can now setup the network manually.

![Network settings](screenshots/network.png)

### Online repositories

![Online Repositories can be enabled](screenshots/onlinerepo.png)

Here you can setup online-repositories. If you enable these you will not need
to update after the installation.

#### Choose the enabled Repositories

![Choosing the default Online Repositories](screenshots/reposchoose.png)

If you enabled the online repositories you can choose which you want. You can
leave this at default values. We will add another repository after the
installation is completed.

### Role

![Choosing the right role for our installation](screenshots/role.png)

You can choose the role of your installation. This will install packages based
on the needs of the role. Since we want a minimal install we choose 'server'

### Partitioning

![Partitioning](screenshots/partitions.png)

This is where we setup partitioning. We don't have any special needs for this
so we will use an even simpler setup then the default. For that click on
*Expert Partitioner* and choose *Start with Existing Partitions*

In the following Window we can simply delete all Partitions by clicking *Edit*
and then *Delete*.

Now that we have a empty hard drive we can create out own partitions. First we
will choose *Add Partition*.

#### Creating a 2G swap Partition

In the new window we create a partition of *custom size* 2G.

![creating a swap partition](screenshots/swap.png)

Now we choose swap in the menu.

![choosing swap](screenshots/swap2.png)

Check that everything is setup correctly and continue with 'next'

![check the swap partition](screenshots/swap3.png)

#### Creating the Root partition

Now we create a Root partition.

![Create a new partition with the empty space](screenshots/rootpart.png)

This time we choose *Operating system* instead of swap.

![choosing swap](screenshots/swap2.png)

In the overview we change the File system to *ext4*.

We also make sure that the mount point is set to */* that is our root file
system.

If everything is correct the overview should show this setup being created.

![overview of the created partitions](screenshots/rootpart2.png)

After confirming we will be back at the Disk Setup window. Here you can check
one last time before the layout will be written to the disk.

![Disk Setup](screenshots/part-overview.png)

### Choosing a timezone

![Choose your timezone](screenshots/tz.png)

Choose you timezone and continue.

### The user setup

![skip the user creation](screenshots/skipuser.png)

We don't need any users at this point so we can simply skip it. If you wish to
create users there will be a tutorial later.

### The root password

![Create a root password](screenshots/rootpw.png)

This is the password that will be needed to login.

You don't need to import any SSH keys.

### Disable SSH key import

![We don't need to import SSH keys](screenshots/sshkeys.png)

Since we don't need SSH keys we won't import them.

It is possible that you can not disable this setting, but we we can disable it
in the next step.

### Overview

![Check the overview for errors](screenshots/overview2.png)

Check for errors in the overview and if necessary disable the SSH Key import
by clicking *Import SSH Host Keys and Configuration*.

After that you can start the *install*.

### Wait for the installation to finish

![The installation process](screenshots/wait.png)

All that is left is to wait for the install to finish.

## Changes on the installed system

Login as the user 'root' and your chosen root password.

### Enabling the SSH server

To be able to log onto the system remotely, the SSH service has to be started
at system startup.

```bash
systemctl enable --now sshd.service
```

### Installing packages

#### Adding the EtherLab repository

To add the [EtherLab](https://build.opensuse.org/project/show/science:EtherLab)
repository please execute the following steps:

```bash
zypper addrepo https://download.opensuse.org/repositories/science:/EtherLab/openSUSE_Leap_15.3 EtherLab
zypper refresh
```

You have to trust the key of the added repository either temporarily or
always. Now we can start installing packages after we update our operating
system to be up-to-date.

#### Installing additional packages

To run realtime programs with EtherCAT functionality, the following packages
are needed.

```bash
zypper update
zypper install ethercat pdserv kernel-rt ethercat-kmp-rt libethercat1 libpdserv3_2
```

If you want to develop your application software locally, the following
packages are needed additionally.

```bash
zypper install git cmake gcc-c++ libethercat-devel pdserv-devel
```

If you want graphics on the system you can also install the X11 server and
[Testmanager](https://www.etherlab.org/en/testmanager/index.php).

```bash
zypper install xorg-x11-server testmanager-ng
```

Since [pre-commit](https://pre-commit.com) is used in the Git repository of
this template project, it should be installed with the following command.

```bash
pip3 install pre-commit
```

### Setting up the realtime kernel as default

We need real-time support so we need to choose 'kernel-rt' as our default
kernel. We will do this over yast, the control center of the
OpenSUSE-Distribution.

```
yast
```

![Yast Menu](screenshots/yast.png)

Now we choose *System* and then select *Boot Loader* by pressing tab on your
keyboard.

![Bootloader Options](screenshots/bootloader.png)

Choose *Bootloader Options* and you can choose the realtime kernel under
*default boot section*

You can also reduce the timeout wait - the time during boot where you can
choose the operating system - to a lower number.

Confirm your changes with *Ok* and then *Quit*

### Disabling the Firewall


Assumed we are on a privyte network, we can disable the firewaal compelety to
be sure, that all services can be accessed from within the site network.

```
systemctl disable --now firewalld.service
```

### Configuring the EtherCAT master

We need to setup the /etc/ethercat.conf. Since we are using the ==ccat==
driver from Beckhoff and we know, that there is only one hardware device
present, we can use the wildcard MAC address for device identification.

```bash
sed -i 's/MASTER0_DEVICE=""/MASTER0_DEVICE="ff:ff:ff:ff:ff:ff"/' /etc/ethercat.conf
```

This makes sure the first offered device will be accepted for use with the
EtherCAT master.

```bash
sed -i 's/DEVICE_MODULES=""/DEVICE_MODULES="ccat"/' /etc/ethercat.conf
```

To ensure the correct device modules are used.

```bash
systemctl enable --now ethercat.service
```
